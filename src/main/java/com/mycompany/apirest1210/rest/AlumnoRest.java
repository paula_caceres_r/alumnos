/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apirest1210.rest;

import com.mycompany.apirest1210.dao.AlumnosJpaController;
import com.mycompany.apirest1210.dao.exceptions.NonexistentEntityException;
import com.mycompany.apirest1210.entity.Alumnos;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author paula
 */
@Path("alumno")
public class AlumnoRest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listaAlumnos() {

        AlumnosJpaController dao = new AlumnosJpaController();

        List<Alumnos> lista = dao.findAlumnosEntities();
        return Response.ok(200).entity(lista).build();

    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Alumnos alumno) {

        AlumnosJpaController dao = new AlumnosJpaController();

        try {
            dao.create(alumno);
        } catch (Exception ex) {
            Logger.getLogger(AlumnoRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(alumno).build();

    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizar(Alumnos alumno) {

        AlumnosJpaController dao = new AlumnosJpaController();

        try {
            dao.edit(alumno);
        } catch (Exception ex) {
            Logger.getLogger(AlumnoRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(alumno).build();

    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{rut}")
    public Response eliminar(@PathParam("rut") String rut) {

        AlumnosJpaController dao = new AlumnosJpaController();

        try {
            dao.destroy(rut);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(AlumnoRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok("Alumno eliminado").build();

    }
    
    
    
    

}
